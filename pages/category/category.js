// pages/category/category.js

var http = require("../../utils/http.js");
var config = require("../../utils/config.js");

Page({

  /**
   * 页面的初始数据
   */
  data: {
    selIndex: 0,
    productList: [],
    categoryImg: '',
    prodid:'',
    MainCur: 0,
    VerticalNavTop: 0,
    load: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   
   
        this.getProdList()
      
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
   


  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },


  tabSelect(e) {
   var index = e.currentTarget.dataset.index;
   this.setData({
      selIndex: index,
      MainCur: index,
      VerticalNavTop: (e.currentTarget.dataset.id - 1) * 50
    });
  },

  // 跳转搜索页
  toSearchPage: function () {
    wx.navigateTo({
      url: '/pages/search-page/search-page',
    })
  },
  getProdList() {
    //加载分类列表
    var params = {
      url: "/prod/ListProd",
      method: "GET",
      callBack: (res) => {
        // console.log(res);
        this.setData({
          productList: res,
        });
      }
    };
    http.request(params);
  },

//跳转商品详情页
  toProdPage: function (e) {
    var prodid = e.currentTarget.dataset.prodid;
    wx.navigateTo({
      url: '/pages/prod/prod?prodid=' + prodid,
    })
  },

  VerticalMain(e) {
    let that = this
    let list = this.data.productList
    let tabHeight = 0
    if (this.data.load) {
      for (let i = 0; i < list.length; i++) {
        let view = wx.createSelectorQuery().select("#main-" + i)
        view.fields({
          size: true
        }, data => {
          list[i].top = tabHeight
          tabHeight = tabHeight + data.height
          list[i].bottom = tabHeight
        }).exec()
      }
      that.setData({
        load: false,
        productList: list
      })
    }
    let scrollTop = e.detail.scrollTop + 50
    for (let i = 0; i < list.length; i++) {
      if (scrollTop > list[i].top && scrollTop < list[i].bottom) {
        that.setData({
          VerticalNavTop: (i - 1) * 50,
          selIndex: i
        })
        return false
      }
    }
  }

})